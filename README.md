# etl_pinlocations

This repository is responsible for attaching spatial and address data to 14-digit PINs using simple spatial joins. It retrieves PIN locations from the latest parcel shapefile (resaved here as GeoJSON), property addresses from BoT/County GIS, and spatial data from multiple sources (see scripts). Spatial datasets are cleaned, preprocessed, and saved in the `data/` directory for joining.

The result of this script is saved for internal use at the CCAO in the `DTBL_PINLOCATIONS` SQL table. Going forward, these scripts will be used to append any new PINs (and their joined data) to that table. The end result will be a table of all 14-digit PINs, even historical ones, with the latest spatial data attached.

This script also creates the [Cook County Assessor's Property Locations](https://datacatalog.cookcountyil.gov/Property-Taxation/Cook-County-Assessor-s-Property-Locations/c49d-89sn) data set on the Cook County Data Portal. This data set is a full join combining the PIN locations data with CCAO's internal `PROPLOCS` table, which contains physical addresses for each PIN. It is stored internally as a SQL view, the defitinition of which is found [here](https://gitlab.com/ccao-data-science---modeling/data-architecture/-/blob/master/code.sql/CCAODATA/VW_PINGEO.sql).
