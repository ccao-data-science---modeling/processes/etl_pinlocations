### COMPILE COOK COUNTY SCHOOL BOUNDARY DATA ###
# Links to datasets in comments
# For each dataset: 1) load 2) standardize column names 3) select relevant columns

# Unit districts (same for elementary and high school) from Cook County
# https://datacatalog.cookcountyil.gov/GIS-Maps/Historical-ccgisdata-Unit-School-Tax-Districts-201/594e-g5w3
unit_districts <- read_sf(here("data", "schools", "unit_districts.geojson")) %>%
  filter(max_agency != "BOARD OF EDUCATION") %>%
  select(max_agency, geometry)

# Cook County elementary schools (outside of Chicago)
# https://datacatalog.cookcountyil.gov/GIS-Maps/Historical-ccgisdata-Elementary-School-Tax-Distric/an6r-bw5a
cook_elem <- read_sf(here("data", "schools", "cook_elem.geojson")) %>%
  rename(elem_district = agency_des) %>%
  select(elem_district, geometry)

# Cook County high schools (outside of Chicago)
# https://datacatalog.cookcountyil.gov/GIS-Maps/Historical-ccgisdata-High-School-Tax-Dist-2016/h3xu-azvs
cook_hs <- read_sf(here("data", "schools", "cook_hs.geojson")) %>%
  rename(hs_district = agency_des) %>%
  select(hs_district, geometry)

# Chicago elementary schools
# https://data.cityofchicago.org/Education/Chicago-Public-Schools-Elementary-School-Attendanc/7edu-z2e8
chi_elem <- read_sf(here("data", "schools", "chicago_elem.geojson")) %>%
  rename(elem_district = school_nm) %>%
  select(elem_district, geometry)

# Chicago high schools
# https://data.cityofchicago.org/Education/Chicago-Public-Schools-High-School-Attendance-Boun/y9da-bb2y
chi_hs <- read_sf(here("data", "schools", "chicago_hs.geojson")) %>%
  rename(hs_district = school_nm) %>%
  select(hs_district, geometry)

# Merge county/city boundaries with unit district boundaries to cover entire county
# Rename unit district column to agree with elementary school datasets
schools_elem <- rbind(
  chi_elem,
  cook_elem,
  unit_districts %>% rename(elem_district = max_agency)
) %>%
  distinct(elem_district) %>%
  st_transform(3435)

# Merge county/city boundaries with unit district boundaries to cover entire county
# Rename district column to agree with high school datasets
schools_hs <- rbind(
  chi_hs,
  cook_hs,
  unit_districts %>% rename(hs_district = max_agency)
) %>%
  distinct(hs_district) %>%
  st_transform(3435)

# Remove extra objects
rm(unit_districts, cook_elem, cook_hs, chi_elem, chi_hs)
